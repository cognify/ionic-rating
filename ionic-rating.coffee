(->
  angular.module('ionic.rating', []).constant('ratingConfig',
    max: 5
    stateOn: null
    stateOff: null).controller('RatingController', [
    '$scope'
    '$attrs'
    'ratingConfig'
    ($scope, $attrs, ratingConfig) ->
      ngModelCtrl = undefined
      ngModelCtrl = $setViewValue: angular.noop

      @init = (ngModelCtrl_) ->
        max = undefined
        ratingStates = undefined
        ngModelCtrl = ngModelCtrl_
        ngModelCtrl.$render = @render
        @stateOn = if angular.isDefined($attrs.stateOn) then $scope.$parent.$eval($attrs.stateOn) else ratingConfig.stateOn
        @stateOff = if angular.isDefined($attrs.stateOff) then $scope.$parent.$eval($attrs.stateOff) else ratingConfig.stateOff
        max = if angular.isDefined($attrs.max) then $scope.$parent.$eval($attrs.max) else ratingConfig.max
        ratingStates = if angular.isDefined($attrs.ratingStates) then $scope.$parent.$eval($attrs.ratingStates) else new Array(max)
        $scope.range = @buildTemplateObjects(ratingStates)

      @buildTemplateObjects = (states) ->
        i = undefined
        j = undefined
        len = undefined
        ref = undefined
        ref = states.length
        j = 0
        len = ref.length
        while j < len
          i = ref[j]
          states[i] = angular.extend({ index: 1 }, {
            stateOn: @stateOn
            stateOff: @stateOff
          }, states[i])
          j++
        states

      $scope.rate = (value) ->
        if !$scope.readonly and value >= 0 and value <= $scope.range.length
          ngModelCtrl.$setViewValue value
          return ngModelCtrl.$render()
        return

      $scope.reset = ->
        $scope.value = ngModelCtrl.$viewValue
        $scope.onLeave()

      $scope.enter = (value) ->
        if !$scope.readonly
          $scope.value = value
        $scope.onHover value: value

      $scope.onKeydown = (evt) ->
        if /(37|38|39|40)/.test(evt.which)
          evt.preventDefault()
          evt.stopPropagation()
          return $scope.rate($scope.value + (if evt.which == 38 or evt.which == 39 then 1: -1 else undefined))
        return

      @render = ->
        $scope.value = ngModelCtrl.$viewValue

      this
  ]).directive 'rating', ->
    {
      restrict: 'EA'
      require: [
        'rating'
        'ngModel'
      ]
      scope:
        readonly: '=?'
        onHover: '&'
        onLeave: '&'
      controller: 'RatingController'
      template: '<ul class="rating" ng-mouseleave="reset()" ng-keydown="onKeydown($event)">' + '<li ng-repeat="r in range track by $index" ng-click="rate($index + 1)"><i class="icon" ng-class="$index < value && (r.stateOn || \'ion-ios-star\') || (r.stateOff || \'ion-ios-star-outline\')"></i></li>' + '</ul>'
      replace: true
      link: (scope, element, attrs, ctrls) ->
        ngModelCtrl = undefined
        ratingCtrl = undefined
        ratingCtrl = ctrls[0]
        ngModelCtrl = ctrls[1]
        if ngModelCtrl
          return ratingCtrl.init(ngModelCtrl)
        return

    }
  return
).call this